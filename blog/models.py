from django.db import models
from django.conf import settings

class Post(models.Model):
    titulo = models.CharField(max_length=255)
    conteudo = models.CharField(max_length=400)
    data_postagem = models.DateTimeField()
    img = models.URLField(max_length=200, null=True)
    criador = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.titulo} ({self.data_postagem})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    data_postagem = models.DateTimeField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

    class Meta:
       ordering = ('-data_postagem',)

class Category(models.Model):
    titulo = models.CharField(max_length=255)
    description = models.CharField(max_length=400)
    post = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.titulo}'