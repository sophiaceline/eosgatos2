from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .temp_data import post_data
from django.urls import reverse, reverse_lazy
from .models import Post, Comment, Category
from django.views import generic
from .forms import PostForm, CommentForm
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView

class PostListView(generic.ListView):
    post = Post
    template_name = 'blog/index.html'

    def get_queryset(self):
        return Post.objects.order_by('-data_postagem')

class PostDetailView(generic.DetailView):
    post = Post
    template_name = 'blog/detail.html'

    def get_queryset(self):
        return Post.objects.all()

class PostCreateView(CreateView):
    form_class = PostForm
    template_name = 'blog/create.html'
    success_url = reverse_lazy('blog:index')

    def form_valid(self, form):
        return super().form_valid(form)

class PostUpdateView(UpdateView):
    form_class = PostForm
    template_name = 'blog/update.html'
    success_url = reverse_lazy('blog:index')

    def form_valid(self, form):
        return super().form_valid(form)

    def get_queryset(self):
        return Post.objects.all()

class PostDeleteView(DeleteView):
    model = Post
    success_url = reverse_lazy('blog:index')
    template_name = 'blog/delete.html'

    def get_object(self, queryset=None):
      post_id = self.request.POST.get('pk')
      return self.get_queryset().filter(pk=post_id).get()

def detail_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    context = {'post': post}
    return render(request, 'blog/detail.html', context)

def list_posts(request):
    post_list = Post.objects.all()
    context = {"post_list": post_list}
    return render(request, 'blog/index.html', context)

def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post_titulo = form.cleaned_data['titulo']
            post_conteudo = form.cleaned_data['conteudo']
            post_data_postagem = form.cleaned_data['data_postagem']
            post_img = form.cleaned_data['img']
            post_criador = form.cleaned_data['criador']
            post = Post(titulo=post_titulo,
                      conteudo=post_conteudo,
                      data_postagem=post_data_postagem,
                      img=post_img,
                      criador=post_criador)
            post.save()
            return HttpResponseRedirect(
                reverse('blog:detail', args=(post.id, )))
    else:
        form = PostForm()
    context = {'form': form}
    return render(request, 'blog/create.html', context)

def update_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.titulo = request.POST['titulo']
            post.conteudo = request.POST['conteudo']
            post.data_postagem = request.POST['data_postagem']
            post.img = request.POST['img']
            post.criador = request.POST['criador']
            post.save()
            return HttpResponseRedirect(
                reverse('blog:detail', args=(post.id, )))
    else:
        form = PostForm(
            initial={
                'titulo': post.titulo,
                'conteudo': post.conteudo,
                'data_postagem': post.data_postagem,
                'img': post.img,
                'criador': post.criador
            })

    context = {'post': post, 'form': form}
    return render(request, 'blog/update.html', context)


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        post.delete()
        return HttpResponseRedirect(reverse('blog:index'))

    context = {'post': post}
    return render(request, 'blog/delete.html', context)

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment_data_postagem= form.cleaned_data['data_postagem']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            data_postagem=comment_data_postagem,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('blog:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'blog/comment.html', context)

def list_categories(request):
    categories_list = Category.objects.all().order_by('titulo')
    context = {"categories_list": categories_list}
    return render(request, 'blog/categories.html', context)

def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'blog/detail_category.html', context)