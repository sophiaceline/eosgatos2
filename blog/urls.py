from django.urls import path

from . import views

app_name = 'blog'
urlpatterns = [
    path('<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path('create/', views.PostCreateView.as_view(), name='create'),
    path('', views.PostListView.as_view(), name='index'),
    path('update/<int:pk>/', views.PostUpdateView.as_view(), name='update'),
    path('delete/<int:post_id>/', views.delete_post, name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('categories', views.list_categories, name='categories'),
    path('categories/<int:category_id>', views.detail_category, name='detail_category'),
]