from django import forms
from django.forms import ModelForm
from .models import Post, Comment

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'titulo',
            'conteudo',
            'data_postagem',
            'img',
            'criador'
        ]
        labels = {
            'titulo': 'Título',
            'conteudo': 'Conteúdo',
            'data_postagem': 'Data',
            'img': 'URL da imagem do post',
            'criador': 'Criador'
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'data_postagem'
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
            'data_postagem':'Data'
        }