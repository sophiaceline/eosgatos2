from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('raças/', views.racas, name='raças'),
    path('mainecoon/', views.mainecoon, name='mainecoon'),
    path('siames/', views.siames, name='siames'),
    path('persa/', views.persa, name='persa'),
]