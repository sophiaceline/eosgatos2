from django.shortcuts import render

def index(request):
    context = {}
    return render(request, 'staticpages/index.html', context)

def about(request):
    context = {}
    return render(request, 'staticpages/about.html', context)

def racas(request):
    context = {}
    return render(request, 'staticpages/raças.html', context)

def mainecoon(request):
    context = {}
    return render(request, 'staticpages/mainecoon.html', context)

def siames(request):
    context = {}
    return render(request, 'staticpages/siames.html', context)

def persa(request):
    context = {}
    return render(request, 'staticpages/persa.html', context)